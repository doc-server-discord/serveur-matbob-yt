# Les niveaux
## Le système
Tu n'es pas sans le savoir, le serveur possède un système de niveaux. C'est pas compliqué. Il suffit de parler et d'intéragir avec les gens. Afin d'éviter l'invasion de messages déstinés à XP, on ne peut recevoir de l'XP toutes les minutes. On peut gagner entre 15 et 25 XP.

## Les grades à gagner
- Niveau 1: Bravo, vous êtes dans la *Matboobs Family*
- Niveau 6: Shinigami
- Niveau 11: Fairy Tail Mage
- Niveau 16: Padawan
- Niveau 21: Pokémon Master
- Niveau 26: Hokage
- Niveau 31: Chapochat
- Si tu as beaucoup de détermination: Niveau 99: *surprise*

Petite note: si tu commences à farmer l'XP comme pas permis (bot avec cron qui poste un message en votre nom toutes les minutes par ex.), tu pourrais te faire derank, ce qui serait fort dommage.

Petite note 2: Si tu es déterminé à te faire un bot qui poste le fameux message, fais juste gaffe avec Discord. S'ils grillent ton script en train d'utiliser un compte "normal", ils forceront ton compte à bot, ce qui serait bien dommage aussi.
