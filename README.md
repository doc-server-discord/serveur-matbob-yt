# MatbobYT
Bienvenue sur le serveur Discord de Matbob !

Si tu es nouveau, je te conseille de [voir les commandes disponibles](commandes.md) ou bien d'[aller jeter un coup d'oeil aux liens utiles](liens.md).

Le serveur implémente aussi un système de niveaux, donc si tu veux savoir comment cela fonctionne, c'est [par icitte](niveaux.md)
